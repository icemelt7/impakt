'use strict';

module.exports = function(Doctor) {
  Doctor.observe('access', function(ctx, next) {
    console.log('Accessing %s matching %s', ctx.Model.modelName, JSON.stringify(ctx.query.where));
    if (ctx.query.where.q) {
      ctx.query.where = {
        name: {like: '%' + ctx.query.where.q + '%'},
      };
    }
    next();
  });
};
